%         figure('Name',file.name);
%         subplot(1,2,1); plot(time,walkingSpeed);
%         title('Walking Speed');
%         xlim([0 time(length(time))]); ylim([0 2.0]);
%         xlabel('Time (s)'); ylabel('Walking Speed (m/s)');
%         
%         subplot(1,2,2); hold on;
%         plot(time,actualStiffness,'b'); 
%         plot(time,expStiffness,'b--'); 
%         ylim([0 50]);
%         title('Stiffness');
%         xlim([0 time(length(time))]);
%         xlabel('Time (s)'); ylabel('Stiffness (N/mm)');
%                 
%         % soft position limits to keep the fulcrum from being too soft and
%         % bottoming out or running off the "stiff" end of the potentiometer
%         stiffnessLimLow = linspace(stiffnessLimit(2),stiffnessLimit(2),length(time))';
%         stiffnessLimHigh = linspace(stiffnessLimit(1),stiffnessLimit(1),length(time))';
%         meanStiffnessVec = linspace(meanStiffness,meanStiffness,length(time))';
%         
%         figure('Name',file.name); hold on;
%         plot(time,actualStiffness,'b');
%         plot(time,expStiffness,'b--');
%         plot(time,stiffnessLimLow,'k-');
%         plot(time,stiffnessLimHigh,'k-');
%         plot(time,meanStiffnessVec, 'k--');
%         xlim([0 time(length(time))]); ylim([0 50]);
%         xlabel('Time (seconds)'); ylabel('Stiffness (N/mm)');
%         legend('Actual Stiffness','Expected Stiffness','Stiffness Limits');
%         
%         treadSpeeds = [0.5 1 1.5 2 2.5 3 3.5]*0.44704;
%         speeds = zeros(1,numStrides);
%         avgWalkingSpeed = zeros(length(time),1);
%         spLimLow = linspace(speedMap(1),speedMap(1),length(time));
%         spLimHigh = linspace(speedMap(2),speedMap(2),length(time));
%         figure('Name',file.name); hold on
%         ms = plot(time,meanSpeed,'linewidth',4);
%         ws = plot(time,walkingSpeed);
% %         ml = plot(time,spLimLow,'k',time,spLimHigh,'k');
%         xlabel('Time (s)'); ylabel('Walking Speed (m/s)');
%         legend('3 stride moving average','raw speed','speed mapping limits');
%         for i=1:length(treadSpeeds)
%             plot(time,linspace(treadSpeeds(i),treadSpeeds(i),length(walkingSpeed)),'k--');
%         end
%         
%         % find number of strides
%         ipt = findchangepts(walkingSpeed,'MaxNumChanges',100000000);
%         meanStrideSpeeds = meanWalkingSpeed(ipt);
%         strideSpeeds = walkingSpeed(ipt);
%         figure; hold on; plot(strideSpeeds); plot(meanStrideSpeeds);
        %%
        % plot the moving average speed and fulcrum position
%         figure; hold on; 
% %         yyaxis left
% %         plot(time, meanWalkingSpeed,'b','LineWidth',3);
% %         yyaxis right
%         plot(time(start:end)-time(start), actualStiffness(start:end), 'k', 'LineWidth',3);
%         plot(time(start:end)-time(start), expStiffness(start:end), 'r', 'LineWidth', 1);
%         
%         figure(100+i); hold on; 
%         plot(time(start:end)-time(start), meanWalkingSpeed(start:end), 'LineWidth', 3);
%         ylim([0 1.6]);
          %% plots for paper
%         % example walking at varrying speeds
%         figure; hold on; 
%         time = time(2601:7801)-time(2601);
%         plot(time,actualStiffness(2601:7801),'k','LineWidth',3);
%         plot(time,expStiffness(2601:7801),'r', 'LineWidth',1.5);
%         legend('Actual Stiffness', 'Target Stiffness');
%         xlim([0, max(time)]);
% 
%         % range speed test
%         figure; hold on; 
%         time = time(1601:2801)-time(1601);
%         plot(time, actualStiffness(1601:2801), 'k', 'LineWidth',3);
%         plot(time, expStiffness(1601:2801), 'r', 'LineWidth', 1);
%         ylim([0 50]);
%         xlabel('Time (s)'); ylabel('Stiffness (N/mm)');
%         legend('Actual Stiffness', 'Target Stiffness');