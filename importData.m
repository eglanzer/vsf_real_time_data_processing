function [A,G,walkingSpeed,fulPos,time,props,desStiffness,desPos] = importData(filename)
%IMPORTDATA imports walking data from real time controller
%
% Example:
%   [walkingSpeed,desiredStiffness,desiredPos,actualPos,time] = importfile('SEN046.DAT')

%% Initialize variables.
delimiter = ',';

%% Format for each line of text:
formatSpec = '%q%q%q%q%q%q%q%q%q%q%q%q%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read values from header
rate = sscanf(fgets(fileID),'LOG RATE: %f Hz');
mass = sscanf(fgets(fileID),'SUBJECT MASS: %f');
lowSpeed = sscanf(fgets(fileID),'LOW SPEED MAP LIMIT: %f');
highSpeed = sscanf(fgets(fileID),'HIGH SPEED MAP LIMIT: %f');
numStrides = sscanf(fgets(fileID),'NUMBER OF STRIDES FOR MEAN WALKING SPEED: %i');
mapDir = sscanf(fgets(fileID),'MAPPING DIRECTION: %i');
svsfState = sscanf(fgets(fileID),'VSF STATE: %i');
sstiffnessSetting = sscanf(fgets(fileID),'STIFFNESS SETTING: %i');


%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines', 9, 'ReturnOnError', false, 'EndOfLine', '\r\n');

%% Close the text file.
fclose(fileID);

%% Convert the contents of columns containing numeric text to numbers.
% Replace non-numeric text with NaN.
raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = dataArray{col};
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));

for col=[1,2,3,4,5,6,7,8,9,10,11,12,13]
    % Converts text in the input cell array to numbers. Replaced non-numeric
    % text with NaN.
    rawData = dataArray{col};
    for row=1:size(rawData, 1);
        % Create a regular expression to detect and remove non-numeric prefixes and
        % suffixes.
        regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
        try
            result = regexp(rawData{row}, regexstr, 'names');
            numbers = result.numbers;
            
            % Detected commas in non-thousand locations.
            invalidThousandsSeparator = false;
            if any(numbers==',');
                thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                if isempty(regexp(numbers, thousandsRegExp, 'once'));
                    numbers = NaN;
                    invalidThousandsSeparator = true;
                end
            end
            % Convert numeric text to numbers.
            if ~invalidThousandsSeparator;
                numbers = textscan(strrep(numbers, ',', ''), '%f');
                numericData(row, col) = numbers{1};
                raw{row, col} = numbers{1};
            end
        catch me
        end
    end
end

%% Allocate imported array to column variable names
% walkingSpeed = dataArray{:,1};
% meanSpeed = dataArray{:,2};
% actualPos = dataArray{:,3};
numericData = numericData(1:length(numericData)-1,:);
A = numericData(:,1:3);
G = numericData(:,4:6);
walkingSpeed = numericData(:,7);
fulPos = numericData(:,8);
desStiffness = numericData(:,9);
desPos = numericData(:,10);
vsfState = numericData(end,11);
stiffnessSetting = numericData(end,12);
maxStiffness = numericData(end,13); 

if stiffnessSetting==1
    mapDir=-1;
elseif stiffnessSetting==2
    mapDir=0; 
elseif stiffnessSetting==3
    mapDir=1;
end

props = [rate; mass; lowSpeed; highSpeed; numStrides; mapDir; vsfState; stiffnessSetting; maxStiffness];


% create time vector based on data length and 200 Hz sampling rate
time(:,1) = 0:0.005:(length(walkingSpeed)-1)/200;

% the last value of actualPos usually gets cut off
% shorten vectors by 1 to match the length of the useful actualPos data
% actualPos = actualPos(1:(length(actualPos)-1),:);
% time = time(1:length(actualPos),:);
% walkingSpeed = walkingSpeed(1:length(actualPos),:);
% meanSpeed = meanSpeed(1:length(actualPos),:);
end



