function [ meanWalkingSpeed ] = MovingAverageSpeed(walkingSpeed,numStrides)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if nargin==1
    numStrides = 3;
end

speeds = zeros(1,3);
meanWalkingSpeed = zeros(length(walkingSpeed),1);

for i=1:length(walkingSpeed)
    if walkingSpeed(i) ~= speeds(1)
		for j=numStrides:-1:2
			speeds(j) = speeds(j-1);
        end
		speeds(1)=walkingSpeed(i);
    end
	if speeds(numStrides-1)~=0
		meanWalkingSpeed(i) = mean(speeds);
    else
		meanWalkingSpeed(i) = walkingSpeed(i);
    end  
end
end

