close all; clear; clc;
%% setup
walkingSpeed = [0:0.05:1.6 1.6:-0.05:0];
minStiff = 10;
maxStiff = 46;
lowSpeed = 0.9;
highSpeed = 1.5;
% ---Change these two see how it works---
mapDirWalking = 1;
mapDirAccel = -1;
% --------------------------------------
mass = 85;
meanStiffness = mass/2.7;

for i=1:length(walkingSpeed)
    if walkingSpeed(i)<lowSpeed
        if mapDirAccel == -1 %faster=softer
            desStiff(i) = linearMap(walkingSpeed(i),0,lowSpeed,maxStiff,minStiff);
        elseif mapDirAccel == 0
            desStiff(i) = meanStiffness;
        elseif mapDirAccel == 1 %faster=stiffer
            desStiff(i) = linearMap(walkingSpeed(i),0,lowSpeed,minStiff,maxStiff);
        end
    else
        if mapDirWalking == -1 %faster=softer
            desStiff(i) = linearMap(walkingSpeed(i),lowSpeed,highSpeed,maxStiff,minStiff);
        elseif mapDirWalking == 0
            desStiff(i) = meanStiffness;
        elseif mapDirWalking == 1 %faster=stiffer
            desStiff(i) = linearMap(walkingSpeed(i),lowSpeed,highSpeed,minStiff,maxStiff);
        end
    end
    if desStiff(i)<minStiff
        desStiff(i)=minStiff;
    elseif desStiff(i)>=maxStiff
        desStiff(i)=maxStiff; 
    end
end
figure; hold on;
yyaxis left
plot(desStiff);
ylim([-42 63]);
ylabel('Stiffness (N/mm)');
yyaxis right
plot(walkingSpeed);
plot(linspace(lowSpeed,lowSpeed,length(walkingSpeed)),'--r');
plot(linspace(highSpeed,highSpeed,length(walkingSpeed)),'--r');
ylim([0 1.8]);
ylabel('Walking Speed (m/s)');

function res = linearMap(x,in_min,in_max,out_min,out_max)
    res = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
end