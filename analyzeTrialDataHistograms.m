clear; close all
load('trialData.mat')

%% look at data
% throw out data below 0.9
speedSet1 = trialData{1,2};

stiffSet1 = trialData{1,3};
stiffSet1(speedSet1<0.9)=NaN;
stiffSet1(speedSet1>1.5)=NaN;
meanStiffSet1 = nanmean(stiffSet1);

time1 = trialData{1,1};
time1(speedSet1>1.5)=[];

speedSet1(speedSet1<0.9)=NaN;
speedSet1(speedSet1>1.5)=NaN;
meanSpeedSet1 = nanmean(speedSet1);

% throw out data below 0.9 and take the mean of what's left
speedSet2 = trialData{2,2}(6100:end);

stiffSet2 = trialData{2,3}(6100:end);
stiffSet2(speedSet2<0.9)=NaN;
stiffSet2(speedSet2>1.5)=NaN;
meanStiffSet2 = nanmean(stiffSet2);

time2 = trialData{2,1}(6100:end)-trialData{2,1}(6100);
time2(speedSet2>2)=NaN;

speedSet2(speedSet2<0.9)=NaN;
speedSet2(speedSet2>1.5)=NaN;
meanSpeedSet2 = nanmean(speedSet2);

% compile data
meanData = [meanSpeedSet1 meanSpeedSet2; meanStiffSet1 meanStiffSet2];

minStiffness = [15.34 15.34];
maxStiffness = [32.0 32.0];
minSpeedCar = [0.9 0.9];
maxSpeedCar = [1.4 1.4];

figure(1); 
% setting 1 (faster=softer)
subplot(2,2,1); hold on; 
plot(trialData{1,1},trialData{1,2},'b','LineWidth',2);
xlim([0 240]);
xl=xlim;
line(xl,minSpeedCar,'LineStyle','--','Color','k'); 
line(xl,[meanSpeedSet1 meanSpeedSet1],'LineStyle','-.','Color','k');
line(xl,maxSpeedCar,'LineStyle','--','Color','k');
ylim([0 1.6]);
title('Walking Speed (faster=softer)');
xlabel('time (s)'); ylabel('Rolling Mean Walking Speed (m/s),n=3');
legend({'Walking Speed','Car Speed Limits','Mean Speed Over 0.9 m/s'});

subplot(2,2,3); hold on; 
plot(trialData{1,1},trialData{1,3},'r','LineWidth',2);
xlim([0 240]);
xl=xlim;
line(xl,minStiffness,'LineStyle','--','Color','k'); 
line(xl,[meanStiffSet1 meanStiffSet1],'LineStyle','-.','Color','k');
line(xl,maxStiffness,'LineStyle','--','Color','k')
ylim([13 35]);
title('Stiffness (faster=softer)'); 
xlabel('time (s)'); ylabel('Stiffness (N/mm)');
legend({'Foot Stiffness','Stiffness Limits','Mean Stiffness'});

% setting 3 (faster=stiffer)
subplot(2,2,2); hold on; 
plot(trialData{2,1}(6100:end)-trialData{2,1}(6100),trialData{2,2}(6100:end),'b','LineWidth',2);
xlim([0 275]);
xl=xlim;
line(xl,minSpeedCar,'LineStyle','--','Color','k');
line(xl,[meanSpeedSet2 meanSpeedSet2],'LineStyle','-.','Color','k');
line(xl,maxSpeedCar,'LineStyle','--','Color','k')
ylim([0 1.6]);
title('Walking Speed (faster=stiffer)');
xlabel('time (s)'); ylabel('Rolling Mean Walking Speed (m/s), n=3');
legend({'Walking Speed','Car Speed Limits','Mean Speed Over 0.9 m/s'});

subplot(2,2,4); hold on; 
plot(trialData{2,1}(6100:end)-trialData{2,1}(6100),trialData{2,3}(6100:end),'r','LineWidth',2);
xlim([0 275]);
xl=xlim;
line(xl,minStiffness,'LineStyle','--','Color','k');
line(xl,[meanStiffSet2 meanStiffSet2],'LineStyle','-.','Color','k');
line(xl,maxStiffness,'LineStyle','--','Color','k')
ylim([13 35]);
title('Stiffness (faster=stiffer)'); 
xlabel('time (s)'); ylabel('Stiffness (N/mm)');
legend({'Foot Stiffness','Stiffness Limits','Mean Stiffness'});


%% histograms of speed and stiffness
speedSet1 = trialData{1,4};
ipt1 = findchangepts(speedSet1,'MaxNumChanges',9999);
speedSingleSet1 = speedSet1(ipt1);
speedSet2 = trialData{2,4};
ipt2 = findchangepts(speedSet2,'MaxNumChanges',9999);
speedSingleSet2 = speedSet2(ipt2);

% stiffnesses
stiffSet1 = trialData{1,3};
stiffSingleSet1 = stiffSet1(ipt1);
stiffSingleSet1(speedSingleSet1<0.9)=[];
stiffSingleSet1(speedSingleSet2>1.45)=[];

stiffSet2 = trialData{2,3};
stiffSingleSet2 = stiffSet2(ipt2);
stiffSingleSet2(speedSingleSet2<0.9)=[];


figure(2); 
nbinsSt=6;
subplot(2,2,3); 
histStiff1 = histogram(stiffSingleSet1,nbinsSt);
title('Stiffness Setting 1 (faster=softer)')
subplot(2,2,4);
histStiff2 = histogram(stiffSingleSet2,nbinsSt);
title('Stiffness Setting 3 (faster=stiffer)')

% speeds
speedSingleSet1(speedSingleSet1<0.9)=[];
speedSingleSet1(speedSingleSet1>1.45)=[];

speedSingleSet2(speedSingleSet2<0.9)=[];
speedSingleSet2(speedSingleSet2>1.45)=[];

nbinsSp =7;
subplot(2,2,1);
histSpeed1 = histogram(speedSingleSet1,nbinsSp);
title('Speed Setting 1 (faster=softer)')
subplot(2,2,2);
histSpeed2 = histogram(speedSingleSet2,nbinsSp);
title('Speed Setting 3 (faster=stiffer)')


