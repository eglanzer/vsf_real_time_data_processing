close all;  clear; clc;
%% Setup
positionMap = [75,3400];
softPosLimit = [100,3350];

minTrialTime = 1; 
%% Load files
path = strcat(pwd,'\Walking Data');
files = dir(strcat(path, '\*.DAT'));
i=1;
for file=files'
    % read file and trial properties from header
    [A,G,walkingSpeed,fulPos,time,props,desStiff,desPos] = importData(file.name);
    
    if time(length(time))>minTrialTime %doesn't analyze the data set if it is shorter than a threshold time
        % setup variables
        subjectMass = props(2);
        speedMap = [props(3) props(4)];
        numStrides = props(5);
        mapDir(i) = props(6);
        vsfState(i) = props(7);
        stiffnessSetting(i)=props(8);
        meanWalkingSpeed = MovingAverageSpeed(walkingSpeed,numStrides);
        
        % old black keel
%         minStiffness = 1.1*(-0.0001886*subjectMass^2+0.1366*subjectMass+1.5484);
        % new g10-fr4 keel
        minStiffness = 1.1*(-0.00013072*subjectMass*subjectMass+0.1012*subjectMass+6.2915);
        
        if minStiffness<10
            minStiffness=10;
        end
        maxStiffness = 32;
        
        if vsfState(i)==1 && mapDir(i)==1
            stiffnessMap = [10,32];
            expStiffness = linmap(meanWalkingSpeed,speedMap,[minStiffness,maxStiffness]);          
        elseif vsfState(i)==1 && mapDir(i)==-1
            stiffnessMap = [32,10];
            expStiffness = linmap(meanWalkingSpeed,speedMap,[maxStiffness,minStiffness]);
        elseif vsfState(i)==0
            stiffnessMap = [10,32];
            if stiffnessSetting(i)==1
                expStiffness=linspace(minStiffness,minStiffness,length(time));
            elseif stiffnessSetting(i)==2
                expStiffness=(maxStiffness-minStiffness)/2+minStiffness;
                expStiffness=linspace(expStiffness,expStiffness,length(time));
            elseif stiffnessSetting(i)==3
                expStiffness=linspace(maxStiffness,maxStiffness,length(time));
            end
        end
        
        expStiffness(expStiffness < minStiffness) = minStiffness;
        expStiffness(expStiffness > maxStiffness) = maxStiffness;
        actualStiffness = linmap(fulPos,positionMap,[max(stiffnessMap),min(stiffnessMap)]);        
        
        start = find(meanWalkingSpeed>0,1);
        time = time-time(start);
        trialData{i,1} = time(start:end);
        trialData{i,2} = meanWalkingSpeed(start:end);
        trialData{i,3} = actualStiffness(start:end); 
        trialData{i,4} = walkingSpeed(start:end);

        
        %% end analysis for file
        % print the file name and length of trial
        disp([file.name ' - Trial Length: ' num2str(round(time(length(time))/60,2)) ' min'])
        fclose('all');
        i=i+1;
    end
end

